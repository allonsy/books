# books

My bookreading tracker

This repo maintains my reading list

Most of the info here is in the issues
each issue is a book that is on my list

For the scrum board, the staging column is an ordered list of books of what I'm planning on reading next

The backlog is the unordered list of books that I want to read


# Contributing
Feel free to suggest books via issues!